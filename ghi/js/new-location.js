
window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/states/";
    const response = await fetch(url);
    // if (!response.ok) {
    //     console.log("error binch!")
    // } else {
        const data = await response.json();
        const selectTag = document.getElementById('state');
        for (let state of data.states) {
            // creating an option tag for each state's abb and name
            const option = createOption(state.abbreviation, state.name);
            // console.log(option);
            selectTag.innerHTML += option;
    // }
    function createOption(abbreviation, state) {
        return `<option value="${abbreviation}">${state}</option>`;
     }
    }
    });

window.addEventListener('DOMContentLoaded', async () => {

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
    event.preventDefault();


    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
   const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
    method: "post",
    body: json,
    headers: {'Content-Type': 'application/json'}
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
        }

})




});
