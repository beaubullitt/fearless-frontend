window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/locations/";
    const response = await fetch(url);
    const data = await response.json();
    const selectTag = document.getElementById('location');
    for (let location of data.locations) {
        // creating an option tag for each state's abb and name
        const option = createOption(location);
        selectTag.innerHTML += option;
        function createOption(location) {
            return `<option value="${location.id}">${location.id}</option>`;
         }
    }
});

window.addEventListener('DOMContentLoaded', async () => {
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log("🚀", json)
        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {'Content-Type': 'application/json'}
            };
        const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
                }
            })
        });
