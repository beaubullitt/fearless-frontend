function createCard(name, description, pictureUrl, startsUTC, endsUTC, location) {
    return `
    <div class="shadow-lg p-3 mb-5 bg-white rounded">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
        ${startsUTC} " - " ${endsUTC}
      </div>
        </div>
      </div>
    `;
  };

function cardError(someError) {
    return `
    <div class="alert alert-primary" role="alert">
    ${someError};
    </div>
    `
};

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        let i = 0;
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          console.log("🚀Current conference is: ", conference)
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();

            const title = details.conference.name;

            const description = details.conference.description;
            const starts = details.conference.starts;
            const startsUTC = new Date(starts).toLocaleDateString();
            const ends = details.conference.ends;
            const endsUTC = new Date(ends).toLocaleDateString();
            const location = details.conference.location.name;

            const pictureUrl = details.conference.location.picture_url;

            const html = createCard(title, description, pictureUrl, startsUTC, endsUTC, location);
            console.log("🚀", `col-${i%3}`)
            const columnTag = document.getElementById(`col-${i%3}`);
            console.log("🚀", columnTag)
            // += works for innerHTML too
            columnTag.innerHTML += html;
            }
             i += 1;
          }

        }

      } catch (e) {
        const someError = e;
        const irrelevant = cardError(someError);
        const errorTag = document.querySelector(".col-0");
        errorTag.innerHTML = irrelevant;
        console.error(e);
      // Figure out what to do if an error is raised
    }

  });
